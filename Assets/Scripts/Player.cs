﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float speedX = 1.0f;
    Transform transf;

	// Use this for initialization
	void Start()
    {
        transf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update()
    {
        // Move based on Horizontal input
        Vector3 pos = transf.position;
        pos.x += Input.GetAxisRaw("Horizontal") * speedX * Time.deltaTime;
        transf.position = pos;
    }
}
